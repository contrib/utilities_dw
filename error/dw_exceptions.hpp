/*
 * Copyright (C) 1996-2011 Daniel Waggoner
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DW_EXECPTIONS__
#define __DW_EXECPTIONS__

#include <typeinfo>

class dw_exception : public std::exception
{
public:
  dw_exception() throw() { }; 
  dw_exception(const dw_exception& err) throw() { };
  dw_exception& operator=(const dw_exception& err) throw() { };
  
  virtual const char* what() throw() { return "dw: unspecified error"; };
};

class dw_arg_error : public dw_exception
{
public:
  dw_arg_error() throw() : dw_exception() { };

  virtual const char* what() throw() { return "dw: argument error"; };
};

class dw_mem_error : public dw_exception
{
public:
  dw_mem_error() throw() : dw_exception() { };

  virtual const char* what() throw() { return "dw: memory error"; };
};

class dw_fnct_error : public dw_exception
{
public:
  dw_fnct_error() throw() : dw_exception() { };

  virtual const char* what() throw() { return "dw: function error"; };
};

class dw_dim_error : public dw_exception
{
public:
  dw_dim_error() throw() : dw_exception() { };

  virtual const char* what() throw() { return "dw: dimension error"; };
};

class dw_sing_error : public dw_exception
{
public:
  dw_dim_error() throw() : dw_exception() { };

  virtual const char* what() throw() { return "dw: singularity error"; };
};

#endif
