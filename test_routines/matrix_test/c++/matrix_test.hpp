/*
 * Copyright (C) 1996-2011 Daniel Waggoner
 *
 * This free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If you did not received a copy of the GNU General Public License
 * with this software, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MATRIX_TEST__
#define __MATRIX_TEST__

#include <iostream>
//#include <ostream>
#include <math.h>
#include <stdlib.h>

#include "dw_matrix.h"
#include "dw_rand.h"
#include "dw_matrix_rand.h"
#include "dw_std.h"

using namespace std;

// throw values
#define ERR_INDEX_OUT_OF_RANGE      1
#define ERR_MEM_FAILURE             2
#define ERR_STRING_SYNTAX           3
#define ERR_NULL_TEST_SIZES         4
#define ERR_ARG_TYPE                5

// Argument and return types
#define dw_void_type          1
#define dw_PRECISION_type     2
#define dw_int_type           3
#define dw_TVector_type       4
#define dw_TMatrix_type       5
#define dw_int_size_type      6
#define dw_TMatrix_U_type     7
#define dw_TMatrix_L_type     8
#define dw_TMatrix_S_type     9
#define dw_TMatrix_SPD_type  10
#define dw_pInteger_type     11
#define dw_TPermutation_type 12

// Argument errors
#define ARG_ERR_GENERAL     0x01
#define ARG_ERR_SIZE        0x02
#define ARG_ERR_NULL        0x04

//=== TArg ======================================================================
class TArg
{
public:
  virtual int type(void) = 0;
  virtual void Allocate(void) { };
  virtual void Initialize(void) = 0;
};

class TArg_pointer : public TArg
{
protected:
  int owner;
  int null_OK;
  int change_OK;

public:
  TArg_pointer() { owner=0; null_OK=0; change_OK=0; };

  void SetNullOK(int b) { null_OK = b ? 1 : 0; };
  int IsNullOK(void) { return null_OK; };
  void SetChangeOK(int b) { change_OK = b ? 1 : 0; };
  int IsChangeOK(void) { return change_OK; };
  int Owner(void) { return owner; };

  virtual void* address(void) = 0;
  virtual void Free(void) = 0;
  virtual void EquatePointers(TArg_pointer *p_arg) = 0;
  virtual int Changed(void) = 0;
};

class TArg_sized
{
protected:
  int n_sizes;                // number of sizes
  int **size;                 // pointer to size 
  unsigned char *size_char;   // letter used to define size in argument string

public:
  TArg_sized(int n);
  ~TArg_sized();

  int  GetNumberSizes(void) { return n_sizes; };
  void SetSizeReference(int *s, int i) { size[i]=s; };
  int GetSizeReference(int i) { return *(size[i]); };
  void SetSizeChar(char c, int i) { size_char[i]=c; };
  char GetSizeChar(int i) { return size_char[i]; };

  virtual int GetSize(int i) = 0;
};

class TArg_TMatrix : public TArg_pointer, public TArg_sized
{
protected:
  TMatrix arg;
  TMatrix copy;

public:
  TArg_TMatrix() : TArg_sized(2) { arg=(TMatrix)NULL; copy=(TMatrix)NULL; };
  ~TArg_TMatrix() { Free(); };

  void CreateMatrices(int row, int col);
  TMatrix& GetArg(void) { return arg; };
  TMatrix& GetCopy(void) { return copy; };

  // TArg virtual functions
  virtual int type(void) { return dw_TMatrix_type; };
  virtual void Allocate(void) { CreateMatrices(*(size[0]),*(size[1])); };
  virtual void Initialize(void) { if (arg && owner) EquateMatrix(copy,dw_NormalMatrix(arg)); };

  // Pointer virtual functions
  virtual void* address(void) { return arg; };
  virtual void Free(void);
  virtual void EquatePointers(TArg_pointer* p_arg);
  virtual int Changed(void);

  // Size virtual functions
  virtual int GetSize(int i);
};

class TArg_TMatrix_U : public TArg_TMatrix
{
public:
  virtual int type(void) { return dw_TMatrix_U_type; };
  virtual void Initialize(void);
  virtual void EquatePointers(TArg_pointer *p_arg);
};

class TArg_TMatrix_L : public TArg_TMatrix
{
public:
  virtual int type(void) { return dw_TMatrix_L_type; };
  virtual void Initialize(void);
  virtual void EquatePointers(TArg_pointer *p_arg);
};

class TArg_TMatrix_S : public TArg_TMatrix
{
public:
  virtual int type(void) { return dw_TMatrix_S_type; };
  virtual void Initialize(void);
  virtual void EquatePointers(TArg_pointer *p_arg);
};

class TArg_TMatrix_SPD : public TArg_TMatrix_S
{
public:
  virtual int type(void) { return dw_TMatrix_SPD_type; };
  virtual void Initialize(void);
  virtual void EquatePointers(TArg_pointer *p_arg);
};

class TArg_TVector : public TArg_pointer, public TArg_sized
{
private:
  TVector arg;
  TVector copy;

public:
  TArg_TVector() : TArg_sized(1) { arg=(TVector)NULL; copy=(TVector)NULL; };
  ~TArg_TVector() { Free(); };

  void CreateVectors(int dim);
  TVector& GetArg(void) { return arg; };
  TVector& GetCopy(void) { return copy; };

  // TArg virtual functions
  virtual int type(void) { return dw_TVector_type; };
  virtual void Allocate(void) { CreateVectors(*(size[0])); };
  virtual void Initialize(void) { if (arg && owner) EquateVector(copy,dw_NormalVector(arg)); };

  // Pointer virtual functions
  virtual void* address(void) { return arg; };
  virtual void Free(void);
  virtual void EquatePointers(TArg_pointer* p_arg);
  virtual int Changed(void);

  // Size virtual functions
  virtual int GetSize(int i);
};

class TArg_TPermutation : public TArg_pointer, public TArg_sized
{
private:
  TPermutation arg;
  TPermutation copy;

public:
  TArg_TPermutation() : TArg_sized(1) { arg=(TPermutation)NULL; copy=(TPermutation)NULL; };
  ~TArg_TPermutation() { Free(); };

  void CreatePermutations(int dim);
  TPermutation& GetArg(void) { return arg; };
  TPermutation& GetCopy(void) { return copy; };

  // TArg virtual functions
  virtual int type(void) { return dw_TPermutation_type; };
  virtual void Allocate(void) { CreatePermutations(*(size[0])); };
  virtual void Initialize(void);

  // Pointer virtual functions
  virtual void* address(void) { return arg; };
  virtual void Free(void);
  virtual void EquatePointers(TArg_pointer* p_arg);
  virtual int Changed(void);

  // Size virtual functions
  virtual int GetSize(int i);
};

class TArg_pInteger : public TArg_pointer, public TArg_sized
{
private:
  int n;
  int* arg;
  int* copy;

public:
  TArg_pInteger() : TArg_sized(1) { arg=(int*)NULL; copy=(int*)NULL; n=0; };
  ~TArg_pInteger() { Free(); };

  void CreateIntegerArray(int dim);
  int*& GetArg(void) { return arg; };
  int*& GetCopy(void) { return copy; };

  // TArg virtual functions
  virtual int type(void) { return dw_pInteger_type; };
  virtual void Initialize(void);
  virtual void EquatePointers(TArg_pointer* p_arg);

  // Pointer virtual functions
  virtual void* address(void) { return arg; };
  virtual void Free(void);
  virtual void Allocate(void) { CreateIntegerArray(*(size[0])); };
  virtual int Changed(void);

  // Size virtual functions
  virtual int GetSize(int i) { if (i == 0) return n; else throw ERR_INDEX_OUT_OF_RANGE; };
};

class TArg_int : public TArg
{
protected:
  int arg;
  int copy;

public:
  TArg_int() { arg=copy=-1; };

  int& GetArg(void) { return arg; };
  int& GetCopy(void) { return copy; };

  virtual int type(void) { return dw_int_type; };
  void Initialize(void) { arg=copy=(rand() % 10)+1; };
};

class TArg_int_size : public TArg_int, public TArg_sized
{
private:

public:
  TArg_int_size() : TArg_sized(1), TArg_int() { };

  // TArg virtual functions
  virtual int type(void) { return dw_int_size_type; };
  void Initialize(void) { arg=copy=*(size[0]); };

  // Size virtual functions
  virtual int GetSize(int i) { if (i == 0) return arg; else throw ERR_INDEX_OUT_OF_RANGE; };
};

class TArg_PRECISION : public TArg
{
private:
  PRECISION arg;
  PRECISION copy;

public:
  TArg_PRECISION() { arg=copy=-1.0; };

  PRECISION& GetArg(void) { return arg; };
  PRECISION& GetCopy(void) { return copy; };

  virtual int type(void) { return dw_PRECISION_type; };
  void Initialize(void) { arg=copy=dw_gaussian_rnd(); };
};


//=== TReturn ===================================================================
class TReturn 
{
public:
  virtual int type(void) = 0;

  virtual void CleanCall(void) { };
};

class TReturn_pointer : public TReturn
{
protected:
  int owner;

public:
  TReturn_pointer() { owner=0; };
  virtual void* address(void) = 0;
  void SetOwner(int b) { owner=b ? 1 : 0; };
};

class TReturn_void : public TReturn
{
public:
  TReturn_void() { };
  virtual int type(void) { return dw_void_type; };
};

class TReturn_TMatrix : public TReturn_pointer
{
protected:
  TMatrix rtrn;

public:
  TReturn_TMatrix() { rtrn=(TMatrix)NULL; };
  ~TReturn_TMatrix() { if (owner && rtrn) FreeMatrix(rtrn); };
  TMatrix& GetReturn(void) { return rtrn; };

  virtual int type(void) { return dw_TMatrix_type; };
  virtual void* address(void) { return (void*)rtrn; };

  virtual void CleanCall(void)
  { if (owner && rtrn) { FreeMatrix(rtrn); owner=0; rtrn=(TMatrix)NULL; } };
};

class TReturn_TVector : public TReturn_pointer
{
protected:
  TVector rtrn;

public:
  TReturn_TVector() { rtrn=(TVector)NULL; };
  ~TReturn_TVector() { if (owner && rtrn) FreeVector(rtrn); };
  TVector& GetReturn(void) { return rtrn; };

  virtual int type(void) { return dw_TVector_type; };
  virtual void* address(void) { return (void*)rtrn; };

  virtual void CleanCall(void)
  { if (owner && rtrn) { FreeVector(rtrn); owner=0; rtrn=(TVector)NULL; } };
};

class TReturn_TPermutation : public TReturn_pointer
{
protected:
  TPermutation rtrn;

public:
  TReturn_TPermutation() { rtrn=(TPermutation)NULL; };
  ~TReturn_TPermutation() { if (owner && rtrn) FreePermutation(rtrn); };
  TPermutation& GetReturn(void) { return rtrn; };

  virtual int type(void) { return dw_TPermutation_type; };
  virtual void* address(void) { return (void*)rtrn; };

  virtual void CleanCall(void)
  { if (owner && rtrn) { FreePermutation(rtrn); owner=0; rtrn=(TPermutation)NULL; } };
};

class TReturn_int :  public TReturn
{
protected:
  int rtrn;

public:
  TReturn_int() { rtrn=-1; }
  int& GetReturn(void) { return rtrn; };

  virtual int type(void) { return dw_int_type; };
};

class TReturn_PRECISION : public TReturn
{
protected:
  PRECISION rtrn;

public:
  TReturn_PRECISION() { rtrn=-1.0; }
  PRECISION& GetReturn(void) { return rtrn; };

  virtual int type(void) { return dw_PRECISION_type; };
};

//=== TArgList ==================================================================
class TArgList
{
protected:
  int nargs;
  TArg **args;
  char *arg_string;

  int n_minimal_idx;
  int *minimal_idx;        // 1 <= minimal_idx[i] <= test_sizes[i][0].
  int **test_sizes;        // test_sizes[i][j] satisfies 0 <= i < n_minimal_idx and 0 <= j <= test_sizes[i][0].

  int n_sizes;
  int *sizes_idx;          // 0 <= sizes_idx[i] < n_minimal_idx
  int *correct_sizes;      // correct_size[i]=test_sizes[sizes_idx[i]][minimal_idx[sizes_idx[i]]]
  int *actual_sizes;

public:
  TArgList() { nargs=0; args=(TArg**)NULL; arg_string=(char*)NULL; };
  TArgList(char *string);
  ~TArgList();

  int number_args(void) {return nargs; };
  TArg* arg(int i) { return *(args+i); };

  char* GetArgumentString(void) { return arg_string; };
  void PrintArgList(ostream &out);

  int GetNumberSizes(void) { return n_sizes; };
  int GetNumberMinimalIdx(void) { return n_minimal_idx; };
  int* GetCorrectSizes(void) { return correct_sizes; };
  int* GetActualSizes(void) { return actual_sizes; };

  int ResetMinimalIndexes(void);
  int IncrementMinimalIndexes(void);
  void FreeTestSizes(void);
  void SetTestSizes(int **ts);
  void SetTestSizes(int n, int* ts);
  void SetTestSizes(int min, int max);

  virtual void ComputeCorrectSizes();
  virtual void MakeSizeInvalid(int i);
  virtual void Allocate(void);
  virtual void Initialize(void);
  virtual int SizesOK(void);
  virtual int NullOK(void);
  virtual int ArgChanged(void);
  virtual void DefaultTestSizes(void);
  virtual void DefaultTestSizes_Basic(void) { DefaultTestSizes(); };
  virtual void DefaultTestSizes_InvalidSizes(void) { DefaultTestSizes(); };
  virtual void DefaultTestSizes_InvalidNulls(void) { DefaultTestSizes(); };
  virtual void DefaultTestSizes_EqualPointers(void) { DefaultTestSizes(); };
  virtual void DefaultTestSizes_Speed(void) { DefaultTestSizes(); };
};


//=== TArgInc ===================================================================
class TArgInc
{
protected:
  TArgList *args;

public: 
  TArgInc(TArgList* p_args);
  TArgInc(TArgList* p_args, int** ts);
  TArgInc(TArgList* p_args, int min, int max);
  TArgInc(TArgList* p_args, int nts, int* ts);
  virtual void ComputeActualSizes(void);
  virtual int First(void) = 0;
  virtual int Next(void) = 0;
};

class TArgInc_Basic : public TArgInc
{
protected:
  int *valid_null;

public:
  TArgInc_Basic(TArgList* p_args) : TArgInc(p_args) { valid_null=(int*)dw_malloc(args->number_args()*sizeof(int)); };
  TArgInc_Basic(TArgList* p_args, int** ts) : TArgInc(p_args,ts) { valid_null=(int*)dw_malloc(args->number_args()*sizeof(int)); };
  TArgInc_Basic(TArgList* p_args, int min, int max) : TArgInc(p_args,min,max) 
    { valid_null=(int*)dw_malloc(args->number_args()*sizeof(int)); };
  TArgInc_Basic(TArgList* p_args, int nts, int* ts) : TArgInc(p_args,nts,ts)
    { valid_null=(int*)dw_malloc(args->number_args()*sizeof(int)); };
  ~TArgInc_Basic();

  virtual int First(void);
  virtual int Next(void);

  int IncrementValidNull(void);
};

class TArgInc_InvalidSizes : public TArgInc
{
protected:
  int i;

public:
  TArgInc_InvalidSizes(TArgList* p_args) : TArgInc(p_args) { i=0; };
  TArgInc_InvalidSizes(TArgList* p_args, int** ts) : TArgInc(p_args,ts) { i=0; };
  TArgInc_InvalidSizes(TArgList* p_args, int min, int max) : TArgInc(p_args,min,max) { i=0; };
  TArgInc_InvalidSizes(TArgList* p_args, int nts, int* ts) : TArgInc(p_args,nts,ts) { i=0; };

  virtual void ComputeActualSizes(void);
  virtual int First(void);
  virtual int Next(void);
};

class TArgInc_InvalidNulls : public TArgInc
{
protected:
  int i;

public:
  TArgInc_InvalidNulls(TArgList* p_args) : TArgInc(p_args) { i=-1; };
  TArgInc_InvalidNulls(TArgList* p_args, int** ts) : TArgInc(p_args,ts) { i=-1; };
  TArgInc_InvalidNulls(TArgList* p_args, int min, int max) : TArgInc(p_args,min,max) { i=-1; };
  TArgInc_InvalidNulls(TArgList* p_args, int nts, int* ts) : TArgInc(p_args,nts,ts) { i=-1; };

  virtual int First(void);
  virtual int Next(void);
};

class TArgInc_EqualPointers : public TArgInc
{
protected:
  int *actual;
  int **master;
  int threads;

public:
  TArgInc_EqualPointers(TArgList* p_args) : TArgInc(p_args) { SetupEqualPointers(); };
  TArgInc_EqualPointers(TArgList* p_args, int** ts) : TArgInc(p_args,ts) { SetupEqualPointers(); };
  TArgInc_EqualPointers(TArgList* p_args, int min, int max) : TArgInc(p_args,min,max) { SetupEqualPointers(); };
  TArgInc_EqualPointers(TArgList* p_args, int nts, int* ts) : TArgInc(p_args,nts,ts) { SetupEqualPointers(); };
  ~TArgInc_EqualPointers();

  virtual int First(void);
  virtual int Next(void);

  void SetupEqualPointers(void);
  int SetMaster(void);
  int IncrementActual(void);
  void Initialize(void);
};

class TArgInc_Speed : public TArgInc
{

public:
  TArgInc_Speed(TArgList* p_args) : TArgInc(p_args) { };
  TArgInc_Speed(TArgList* p_args, int** ts) : TArgInc(p_args,ts) { };
  TArgInc_Speed(TArgList* p_args, int min, int max) : TArgInc(p_args,min,max) { };
  TArgInc_Speed(TArgList* p_args, int nts, int* ts) : TArgInc(p_args,nts,ts) { };

  virtual int First(void) { args->ResetMinimalIndexes(); ComputeActualSizes(); args->Allocate(); args->Initialize(); return 1; };
  virtual int Next(void) { return 0; };
};


//=== TFunction =================================================================
class TFunction : public TArgList
{
protected:
  TReturn *rtrn;

  TArgInc *inc;

  char *name;
  int **default_test_sizes;

public:
  TFunction(char* name_string, char *rtrn_string, char* arg_string);
  ~TFunction();

  char* GetName(void) { return name; };
  int First(void) { return inc ? inc->First() : 0; };
  int Next(void) { return inc ? inc->Next() : 0; };

  // functions that must be defined
  virtual int CallFunction(void) = 0; 
  virtual PRECISION ResultOK(void) = 0;

  // functions may need to be redefined
  virtual void SetReturnOwner(void);
  virtual int ArgumentsError(void);
  virtual void CleanCall(void) { rtrn->CleanCall(); };

  virtual int TestBasic(int min=-1, int max=-1);
  virtual int TestInvalidSizes(int min=-1, int max=-1);
  virtual int TestInvalidNulls(int min=-1, int max=-1);
  virtual int TestEqualPointers(int min=-1, int max=-1);
  virtual int TestSpeed(int size=10);
};

#endif

/********************************************************************************
Virtual functions in TFunction

CallFunction - Makes the actual call to the function to be tested and calls 
               SetReturnOwner().  Returns one if the call was successful and
               zero otherwise.  This should be boilerplate from 
               matrix_test_instances.cpp and must be defined.

ResultOK - Determines if the function call correctly computed the results.  The
           return value indicates how far the results differ from the true 
           results and is compared to machine epsilon and the square root of
           machine epsilon.  In cases that this notion is not applicable, a 
           return value of 0.0 indicates success and a negative value indicates 
           failure.  This function must be defined.

SetReturnOwner - Is applicable only if the TReturn class is derived from the 
                 TReturn_pointer class.  Determines if the return value of the 
                 function needs to be freeded.  If the returned pointer value is 
                 not equal to the address of any of the pointer arguments passed 
                 to the function, the the return will be freed.  This function 
                 should be redefined if the default rules do not correctly 
                 determine it the returned pointer value needs to be freed.

ArgumentsError - Tries to determine if the passed arguments are valid.  Checks
                 if the sizes are correct via the function SizesOK() and if null 
                 pointers were correctly passed via the function NullOK().  This 
                 function should be redefined if more than sizes and nulls need 
                 to be checked.  To change how sizes or nulls are checked, the 
                 TArgList functions SizesOK() or NullOK() can be changed.  A 
                 return value of 0 indicates no error and a non-zero return 
                 indicates the type of error encounted.

CleanCall - Performs any clean up from the function call if needed.  The default
            function will free the return value if it is a pointer and 
            SetReturnOwner() determines that the pointer need to be freeded.
            Redefine this function to perform special cleanup.

TestBasic
TestInvalidSizes
TestInvalidNulls
TestEqualPointers
TestSpeed
  These functions set up the test cases for the various tests.  They should
  create the appropriate TArgInc class and call the TArgInc function First().
  These functions should be redefined only if a new derived class of TArgInc
  needs to be used.  If only the default sizes need to be changed, redefine
  the appropriate TArgList function DefaultTestSizes(), DefaultTestSizes_Basic(), 
  DefaultTestSizes_InvalidSizes(), DefaultTestSizes_InvalidNulls(), 
  DefaultTestSizes_EqualPointers(), or DefaultTestSizes_Speed().

=================================================================================
Virtual functions in TArgList

Allocate - Allocates memory for each of the arguments by calling TArg function
           Allocate() for each argument.  Should be called before Initialize() is
           called.  Sizes are determined by TArgList member actual_sizes.  This
           function will rarely be redefined.

Initialize - Initializes the arguments by calling TArg function Initialize() for 
             each argument.  Should be called after Allocate().  This function 
             will be redefined only if special processing is required to setup 
             the arguments.

ComputeCorrectSizes - Computes a set of valid sizes from the size information in
                      the TArgList class.  This function must be redefined if 
                      there is not enough information to compute all the sizes.

MakeSizeInvalid - This functions attempts to set an invalid value for a size 
                  value of an argument derived from TArg_size by randomly 
                  changing its value by a small amount.  This function will be
                  redefined if more care is required to produce an invalid size
                  value.   

SizesOK - Returns one if the sizes are all OK as determined by the size 
          information in the TArgList class.  Returns zero otherwise.  This 
          function must redefined if there is not enough information to 
          determine all the sizes.

NullOK - Returns one if all passed null pointers are permissible, as determined 
         by information in the TArg_pointer class.  Returns zero otherwise. This 
         function will rarely be redefined.

ArgChanged - Returns one if the value of one of the arguments derived from 
             TArg_pointer has changed.  This function will rarely be redefined.

DefaultTestSizes
DefaultTestSizes_Basic
DefaultTestSizes_InvalidSizes
DefaultTestSizes_InvalidNulls
DefaultTestSizes_EqualPointers
DefaultTestSizes_Speed
  Determines the default size values that are permissible for each type of test.
  These functions will be redefined is special size values are required.  The
  default size values as defined in DefaultTestSizes() are 1,2,3,7, and 10.
********************************************************************************/

/********************************************************************************
Incrementing Arguments
  The base of all these test routines is the ability to increment through a set
  of argument values, testing the behavior of the function at each step.  Because
  the origins of this test bed was verification of vector and matrix functions,
  the ability to have sized arguments and test many different combinations of 
  sizes was paramount.  Also, dealing with pointer arguments was of prime 
  concern.  

  The basic behavior is controlled through an argument string.  To illustrate 
  this, we consider the matrix function ProductMM().  Its argument string is:

                            "M(m,n)!+;M(m,k);M(k,n)"

  The arguments are separated by semicolons.  The character M denote a matrix
  arguments and the lower case letters in parenthesis give the sizes of the 
  arguments and defines the relationship between the various sizes.  For a full
  list of argument types, see the comments before the function CheckArgString()
  in the file matrix_test.cpp.  The character '!' denotes a pointer argument that
  can be the null pointer and the character '+' denotes a pointer argument that
  can change in value.  The size character can also take the value '?', which
  indicates that the size relationships cannot be determined in this scheme.  
  If the size character '?' appears, then the functions ComputeCorrectSizes() and
  SizesOK() will have to be redefined to handle these sizes.  

  For each distinct size character, k, m, and n in this example, the class 
  TArgList maintains a list of permissible sizes.  The permissible sizes for k
  would be in test_sizes[0], for m in test_sizes[1], and for n in test_sizes[2].
  The number of permissible sizes for the ith size character is stored in
  test_sizes[i][0].  The current value of the ith size character is stored in
  minimal_idx[i] and so it must be the case that 

                   1 <= minimal_idx[i] <= test_sizes[i][0].
  
  The total number of sizes in this example is 6 (three sized arguments each with 
  two sizes).  The array size_idx stores the size character index for each of the
  size parameters.  In this example size_idx[0]=1, size_idx[1]=2, size_idx[2]=1,
  size_idx[3]=0, size_idx[4]=0, and size_idx[5]=2.  The value of each size 
  parameter as given by the current values of the size characters is stored in
  correct_sizes.  Thus

        correct_size[i]=test_sizes[sizes_idx[i]][minimal_idx[sizes_idx[i]]].

  The actual value of each size parameter is given in the actual_sizes array.  It
  is these values that are are used by the TArg function Allocate() when 
  allocating memory for the arguments.  Types derived from TArg_sized store the 
  address of the appropriate elements of the array actual_sizes and thus have 
  access to this information.

  The functions ResetMinimalIndexes() and IncrementMinimalIndex() initialize and
  increment the elements in minimal_idx and the function ComputeCorrectSizes()
  fills the elements of correct_sizes.  The function SizesOK() uses the values
  of correct_sizes and sizes_idx to determine if values in actual_sizes are 
  permissible.  See the function SizesOK() for details since one cannot simply
  compare the value in actual_sizes and correct_sizes.  Also, both SizesOK() and
  ComputeCorrectSizes() must be redefined if any of the size characters are '?'.
   
********************************************************************************/
